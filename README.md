# dmcommunity_2021_02

https://dmcommunity.org/challenge/challenge-feb-2021/

My solution using swi prolog

## Installation

- install swi prolog
- clone this repository
- run the code with "swipl server.pl --fork=false --port=8888"

or simply run the docker image

- docker run -p 8888:8888 -d --name matteoredaelli/dmcommunity_org_2021_02:latest


## Run & test

To test

- get the test file from  https://github.com/DMCommunity/dmcommunity_shared/raw/master/MedicalServices.json
- curl -XPOST -d @MedicalServices.json -H "Accept: application/json" -H "Content-type: application/json" http://localhost:8888/many
