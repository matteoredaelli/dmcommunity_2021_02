FROM swipl

LABEL maintainer="matteo.redaelli@gmail.com"
LABEL version="1.0"
LABEL description="dmcommunity.org: Challenge Feb-2021 - Benchmark Medical Services - https://dmcommunity.org/challenge/challenge-feb-2021/"

WORKDIR /app
COPY . /app

EXPOSE 8888

ENTRYPOINT ["swipl"]
CMD ["/app/server.pl",  "--user=daemon", "--fork=false", "--port=8888"]
