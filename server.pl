:- [rules].

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_files)).
:- use_module(library(http/http_unix_daemon)).
:- use_module(library(http/http_log)).
:- use_module(library(http/http_dyn_workers)).

:- use_module(library(http/json)).
:- use_module(library(http/http_json)).
:- use_module(library(http/json_convert)).

:- http_handler('/one', http_one, []).
:- http_handler('/many', http_many, []).

server(Port):-
    http_server(http_dispatch,
		[ port(Port),
		  workers(2)
		]).

http_one(Request) :-
	http_read_json(Request, DictIn, [json_object(term)]),
	parse_input(DictIn, DictOut),
	reply_json(DictOut).

http_many(Request) :-
	http_read_json(Request, DictIn, [json_object(term)]),
	parse_input_list(DictIn, DictOut),
	reply_json(DictOut).